import 'package:flutter/material.dart';

class ItemServiceCoffee extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(40, 30, 40, 30),
      decoration: BoxDecoration(
          color: Colors.brown[50],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(200),
              bottomRight: Radius.circular(200)),
          boxShadow: [
            BoxShadow(
                color: Colors.brown[400]!, blurRadius: 6, offset: Offset(1, 3))
          ]),
      child: Column(children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(70),
          child: Image.network(
              'https://images.pexels.com/photos/2956954/pexels-photo-2956954.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
              fit: BoxFit.cover,
              height: 100,
              width: 100),
        ),
        const SizedBox(height: 16),
        Text(
          'Casual chocolate coffee shop',
          style: TextStyle(
              fontSize: 18,
              color: Colors.brown[900],
              fontWeight: FontWeight.bold),
        ),
        const SizedBox(height: 20),
        Text(
          'Regular casual coffee (without milk or cream) is low in calories, In Fact, a typical cup of coffee',
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20),
        ),
        const SizedBox(height: 20),
        TextButton(
            onPressed: () {
              print('tes aja');
            },
            child: Text('Explore More'),
            style: ButtonStyle(
                padding: MaterialStateProperty.resolveWith((states) {
              return EdgeInsets.symmetric(horizontal: 30, vertical: 16);
            }), backgroundColor: MaterialStateProperty.resolveWith((states) {
              return Colors.brown[700];
            }), foregroundColor: MaterialStateProperty.resolveWith((states) {
              return Colors.white;
            }), shape: MaterialStateProperty.resolveWith((states) {
              return RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10));
            })))
      ]),
    );
  }
}
