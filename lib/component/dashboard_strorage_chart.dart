import 'package:d_chart/d_chart.dart';
import 'package:flutter/material.dart';

class DashboardStorageChart extends StatelessWidget {
  const DashboardStorageChart({super.key});

  @override
  Widget build(BuildContext context) {
    List storageData = [
      {
        'value': 17,
        'icon': Icons.folder,
        'title': 'Folder',
        'color': Colors.green[900],
      },
      {
        'value': 7,
        'icon': Icons.note,
        'title': 'Document',
        'color': Colors.pink[900],
      },
      {
        'value': 11,
        'icon': Icons.image,
        'title': 'Image',
        'color': Colors.blue[900],
      },
      {
        'value': 9,
        'icon': Icons.cloud,
        'title': 'Cloud',
        'color': Colors.yellow[800],
      },
    ];

    return Padding(
      padding: EdgeInsets.only(top: 24),
      child: AspectRatio(
          aspectRatio: 16 / 9,
          child: DChartBarCustom(
              radiusBar: BorderRadius.circular(8),
              spaceBetweenItem: 20,
              listData: storageData.map((e) {
                return DChartBarDataCustom(
                    value: e['value'].toDouble(),
                    label: e['title'],
                    color: e['color'],
                    showValue: true,
                    valueCustom: Transform.translate(
                      offset: Offset(0, -24),
                      child: Align(
                          alignment: Alignment.topCenter,
                          child: Column(
                            children: [
                              Text('${e['value']}GB'),
                              const SizedBox(
                                height: 20,
                              ),
                              Icon(
                                e['icon'],
                                color: Colors.white,
                              ),
                            ],
                          )),
                    ));
              }).toList())),
    );
  }
}
