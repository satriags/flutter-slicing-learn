// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'component/item_service_coffee.dart' as ItemCoffee;

class ItemProductCoffee extends StatelessWidget {
  const ItemProductCoffee({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 180,
      decoration: BoxDecoration(
          color: Colors.brown[50], borderRadius: BorderRadius.circular(12)),
      child: Column(
        children: [
          ClipRRect(
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                  bottomLeft: Radius.circular(100),
                  bottomRight: Radius.circular(100)),
              child: Image.network(
                  'https://images.pexels.com/photos/312418/pexels-photo-312418.jpeg',
                  height: 220,
                  width: 180,
                  fit: BoxFit.cover)),
          // const SizedBox(height: 16),
          Padding(
              padding: const EdgeInsets.all(16),
              child: Column(children: [
                Text('Casual chocolate coffee',
                    style: TextStyle(
                        color: Colors.brown[700],
                        fontSize: 13,
                        fontWeight: FontWeight.bold)),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('\$30.00',
                        style: TextStyle(
                            color: Colors.deepOrange[900],
                            fontSize: 14,
                            fontWeight: FontWeight.bold)),
                    const SizedBox(width: 8),
                    Text('\$36.00',
                        style: TextStyle(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.brown[700],
                            fontSize: 12,
                            fontWeight: FontWeight.bold))
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RatingBar.builder(
                      initialRating: 4.5,
                      minRating: 1,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemSize: 12,
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {},
                      ignoreGestures: true,
                    ),
                    const SizedBox(width: 8),
                    Text('3.2k review',
                        style: TextStyle(color: Colors.brown, fontSize: 12))
                  ],
                ),
                const SizedBox(height: 24),
                TextButton(
                    onPressed: () {
                      print('shop now');
                    },
                    child: Text('Shop Now'),
                    style: ButtonStyle(
                        padding: MaterialStateProperty.resolveWith((states) {
                      return EdgeInsets.symmetric(horizontal: 30, vertical: 16);
                    }), backgroundColor:
                            MaterialStateProperty.resolveWith((states) {
                      return Colors.brown[700];
                    }), foregroundColor:
                            MaterialStateProperty.resolveWith((states) {
                      return Colors.white;
                    }), shape: MaterialStateProperty.resolveWith((states) {
                      return RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10));
                    })))
              ]))
        ],
      ),
    ));
  }
}
