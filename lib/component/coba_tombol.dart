import 'package:flutter/material.dart';
import 'component/item_image_storage.dart';
import 'component/item_product_coffee.dart';
import 'component/item_service_coffee.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(
        builder: (context) => Scaffold(
          appBar: AppBar(
            title: const Text('Item Service Coffee'),
            backgroundColor: Colors.brown,
            titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
          ),
          body: MyAppBody(),
        ),
      ),
    );
  }
}

class MyAppBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(padding: const EdgeInsets.all(16), children: [
      ButtonTombol(
        nameButton: 'Item Storage Page',
        titlePage: 'Item Storage Layer',
        targetPage: ItemStorageDashboard(),
      ),
      ButtonTombol(
        nameButton: 'Item Product Page',
        titlePage: 'Item Product Layer',
        targetPage: ItemProductCoffee(),
      ),
      ButtonTombol(
        nameButton: 'Item Service Page',
        titlePage: 'Item Service Layer',
        targetPage: ItemServiceCoffee(),
      ),
    ]);
  }
}

class ButtonTombol extends StatelessWidget {
  final String nameButton;
  final String titlePage;
  final Widget targetPage;

  const ButtonTombol(
      {Key? key,
      required this.nameButton,
      required this.targetPage,
      required this.titlePage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          TextButton(
            onPressed: () {
              _navigateToPage(context, targetPage, titlePage);
            },
            child: Text(nameButton),
          ),
        ],
      ),
    );
  }

  void _navigateToPage(BuildContext context, Widget page, String titlePage) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => _buildWrapperPage(context, page, titlePage),
      ),
    );
  }

  Widget _buildWrapperPage(
      BuildContext context, Widget page, String titlePage) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            (titlePage) ?? 'Default Title',
          ),
          backgroundColor: Colors.brown,
          titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
        ),
        body: ListView(padding: const EdgeInsets.all(16), children: [
          page,
        ]));
  }
}
