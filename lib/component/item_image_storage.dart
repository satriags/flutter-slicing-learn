// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
// import 'component/item_service_coffee.dart' as ItemCoffee;

class ItemStorageDashboard extends StatelessWidget {
  const ItemStorageDashboard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      margin: const EdgeInsets.fromLTRB(0, 0, 0, 20),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6,
            )
          ],
          borderRadius: BorderRadius.circular(12)),
      child: Row(
        children: [
          Icon(Icons.image, color: Colors.purple),
          const SizedBox(width: 30),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Image Storage',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Loading',
                    style: TextStyle(color: Colors.grey),
                  ),
                  Text(
                    '44%',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              const SizedBox(height: 4),
              Stack(children: [
                Container(
                  width: double.infinity,
                  height: 6,
                  decoration: BoxDecoration(
                      color: Colors.purple[50],
                      borderRadius: BorderRadius.circular(30)),
                ),
                Container(
                  width: 80,
                  height: 6,
                  decoration: BoxDecoration(
                      color: Colors.purple[900],
                      borderRadius: BorderRadius.circular(30)),
                )
              ])
            ],
          )),
        ],
      ),
    );
  }
}
